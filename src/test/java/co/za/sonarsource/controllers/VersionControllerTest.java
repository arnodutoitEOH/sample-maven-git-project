package co.za.sonarsource.controllers;

class VersionControllerTest {

  //  @Test
  //  void versionInformation() {
  //    VersionController versionController = new VersionController();
  //
  //    String returnString = versionController.versionInformation();
  //    assertTrue(returnString.contains("git.total.commit.count"));
  //  }
  //
  //  @Test
  //  void readGitPropertiesNull() {
  //
  //    VersionController versionController = new VersionController();
  //    versionController.setGitPropertiesFile(null);
  //
  //    String returnString = versionController.readGitProperties();
  //    assertTrue(returnString.contains("Version information could not be retrieved"));
  //  }
  //
  //  @Test
  //  void readGitPropertiesNotExists() {
  //
  //    VersionController versionController = new VersionController();
  //    versionController.setGitPropertiesFile("gitproperties.properties");
  //
  //    String returnString = versionController.readGitProperties();
  //    assertTrue(returnString.contains("Version information could not be retrieved"));
  //  }
  //
  //  @Test
  //  void readGitProperties() {
  //
  //    VersionController versionController = new VersionController();
  //
  //    String returnString = versionController.readGitProperties();
  //    assertTrue(returnString.contains("git.total.commit.count"));
  //  }
  //
  //  @Test
  //  void readFromInputStream() {
  //    String inputString =
  //        "{\n"
  //            + "  \"git.branch\" : \"master\",\n"
  //            + "  \"git.build.host\" : \"arnodt-PC\",\n"
  //            + "  \"git.build.time\" : \"2019-12-13T14:03:13+0200\",\n"
  //            + "  \"git.build.user.email\" : \"arno.dutoit@eoh.com\",\n"
  //            + "  \"git.build.user.name\" : \"Arno du Toit\",\n"
  //            + "  \"git.build.version\" : \"0.0.1-SNAPSHOT\",\n"
  //            + "  \"git.closest.tag.commit.count\" : \"\",\n"
  //            + "  \"git.closest.tag.name\" : \"\",\n"
  //            + "  \"git.commit.id\" : \"eaafb0156e11261ca809ceda8479c80891335dd4\",\n"
  //            + "  \"git.commit.id.abbrev\" : \"eaafb01\",\n"
  //            + "  \"git.commit.id.describe\" : \"eaafb01\",\n"
  //            + "  \"git.commit.id.describe-short\" : \"eaafb01\",\n"
  //            + "  \"git.commit.message.full\" : \"Removed warning in sonar goal\",\n"
  //            + "  \"git.commit.message.short\" : \"Removed warning in sonar goal\",\n"
  //            + "  \"git.commit.time\" : \"2019-12-13T13:54:22+0200\",\n"
  //            + "  \"git.commit.user.email\" : \"arno.dutoit@eoh.com\",\n"
  //            + "  \"git.commit.user.name\" : \"Arno du Toit\",\n"
  //            + "  \"git.dirty\" : \"false\",\n"
  //            + "  \"git.local.branch.ahead\" : \"3\",\n"
  //            + "  \"git.local.branch.behind\" : \"0\",\n"
  //            + "  \"git.remote.origin.url\" :
  // \"https://arnodutoitEOH@bitbucket.org/arnodutoitEOH/sample-maven-git-project.git\",\n"
  //            + "  \"git.tags\" : \"\",\n"
  //            + "  \"git.total.commit.count\" : \"4\"\n"
  //            + "}";
  //
  //    InputStream inputStream =
  //        new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
  //
  //    VersionController versionController = new VersionController();
  //
  //    try {
  //      String returnString = versionController.readFromInputStream(inputStream);
  //      assertTrue(returnString.contains("git.total.commit.count"));
  //    } catch (IOException e) {
  //      fail();
  //    }
  //  }
}
