package co.za.sonarsource;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.web.annotation.WebEndpoint;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@WebEndpoint(id = "versioninfo", enableByDefault = true)
public class VersionInfoEndPoint {

  @Autowired private BuildProperties buildProperties;
  @Autowired private GitProperties gitProperties;
  @Autowired private Environment environment;

  @SneakyThrows
  private Object findInfo(String info) {
    switch (info) {
      case "build-info":
        return this.buildProperties;
      case "git-info":
        return this.gitProperties;
      case "profile-info":
        return new ProfileInfo(
            this.environment.getActiveProfiles(), this.environment.getDefaultProfiles());
      case "change-info":
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("changelog.json");

        List<ChangeLog> changeLog = ChangeLog.parseInputStreamAsJson(inputStream);

        if (inputStream != null) {
          inputStream.close();
        }

        return changeLog;
    }

    return "";
  }

  @ReadOperation
  public Map<String, Object> versionInfo() {
    Map<String, Object> versionInfo = new HashMap<>();

    versionInfo.put("build-info", findInfo("build-info"));
    versionInfo.put("git-info", findInfo("git-info"));
    versionInfo.put("profile-info", findInfo("profile-info"));
    versionInfo.put("change-info", findInfo("change-info"));

    return versionInfo;
  }

  @ReadOperation
  public Object versionInfo(@Selector String option) {
    return findInfo(option);
  }
}
