package co.za.sonarsource;

import lombok.Getter;
import lombok.Setter;

public class GitTag {
  @Getter @Setter private String name;
}
