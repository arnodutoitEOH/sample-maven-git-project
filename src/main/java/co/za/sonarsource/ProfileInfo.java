package co.za.sonarsource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

public class ProfileInfo {
  @Getter private List<String> activeProfiles;
  @Getter private List<String> defaultProfiles;

  public ProfileInfo(List<String> activeProfiles, List<String> defaultProfiles) {
    this.activeProfiles = new ArrayList<>(activeProfiles);
    this.defaultProfiles = new ArrayList<>(defaultProfiles);
  }

  public ProfileInfo(String[] activeProfiles, String[] defaultProfiles) {
    this.activeProfiles = Arrays.asList(activeProfiles);
    this.defaultProfiles = Arrays.asList(defaultProfiles);
  }
}
