package co.za.sonarsource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleMavenGitProjectApplication {

  public static void main(String[] args) {
    SpringApplication.run(SampleMavenGitProjectApplication.class, args);
  }
}
