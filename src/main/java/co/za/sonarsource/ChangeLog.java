package co.za.sonarsource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import io.micrometer.core.instrument.util.IOUtils;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class ChangeLog {
  @Getter @Setter private String id;
  @Getter @Setter private String message;
  @Getter @Setter private String authorName;
  @Getter @Setter private String authorEmail;
  @Getter @Setter private String committerName;
  @Getter @Setter private String committerEmail;
  @Getter @Setter private String date;
  @Getter @Setter private List<GitTag> tags = new ArrayList<>();

  public static List<ChangeLog> parseInputStreamAsJson(InputStream stream) {

    try {
      String json = IOUtils.toString(stream, StandardCharsets.UTF_8);
      ObjectMapper objectMapper = new ObjectMapper();
      CollectionType collectionType =
          objectMapper.getTypeFactory().constructCollectionType(List.class, ChangeLog.class);
      return objectMapper.readValue(json, collectionType);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    return new ArrayList<>();
  }
}
